#!/usr/bin/env python

import requests
from bs4 import BeautifulSoup
from time import time
import pymysql
import pymysql.cursors
import sys
import re
from fake_useragent import UserAgent

start_time = time()

linkIndex = 1

for x in range(10):
    print("\n")
    restListLink = "https://www.dianping.com/search/category/299/10/p" + str(linkIndex)
    print(restListLink)
    ua1 = UserAgent()
    randomHeader = {'User-Agent':str(ua1.random)}
    page = requests.get(restListLink, randomHeader)
    print(page.status_code)
    soup = BeautifulSoup(page.content, 'html.parser')
    for y in range(15):
        print('\n')
        restNameList = len(soup.find_all('a', {'data-hippo-type':'shop'}))
        for j in range(restNameList):
            phy = soup.find_all('a', {'data-hippo-type':'shop'})[j].get_text()
            print("Name: ", phy)

    linkIndex += 1

end_time = time()
time_taken = end_time - start_time

print("Total time taken in seconds: ", time_taken)
