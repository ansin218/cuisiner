#!/usr/bin/env python

import requests
from bs4 import BeautifulSoup
from time import time
import pymysql
import pymysql.cursors
import sys
import re
from fake_useragent import UserAgent

start_time = time()

db = pymysql.connect(host='localhost', user='root', password='password', db='apperitivo', autocommit=True, use_unicode=True, charset="utf8")
cursor = db.cursor()

linkIndex = 0

for x in range(91):
    print("\n")
    restListLink = "https://www.tripadvisor.com/Restaurants-g187309-oa" + str(linkIndex) + "-Munich_Upper_Bavaria_Bavaria.html#EATERY_LIST_CONTENTS"
    print(restListLink)
    ua1 = UserAgent()
    randomHeader = {'User-Agent':str(ua1.random)}
    page = requests.get(restListLink, randomHeader)
    soup = BeautifulSoup(page.content, 'html.parser')
    for y in range(30):
        print('\n')
        restNameList = soup.find_all('a', class_='property_title')[y].get_text()
        restNameList = restNameList.replace("\n", "")
        try:
            restNameList = re.escape(restNameList)
        except TypeError:
            restNameList = restNameList
        restLink = soup.find_all('a', class_='property_title')[y].get('href')
        restLink = "https://www.tripadvisor.com" + restLink
        ua2 = UserAgent()
        randomHeader = {'User-Agent':str(ua2.random)}
        reviewCountPage = requests.get(restLink, randomHeader)
        reviewCountSoup = BeautifulSoup(reviewCountPage.content, 'html.parser')
        try:
            reviewPageNumber = reviewCountSoup.find_all('span', class_='last')[0].get_text()
            starCount = reviewCountSoup.find_all('span', class_='overallRating')[0].get_text()
            cuisineNodes = len(reviewCountSoup.find_all('div', class_='text'))
            if cuisineNodes > 0:
                for c in range(cuisineNodes):
                    cN = reviewCountSoup.find_all('div', class_='text')[c].get_text()
                    if cN.startswith(' '):
                        cuisineType = cN
                    else:
                        cuisineType = 'Lookup'
            else:
                cuisineType = 'Lookup'
            if int(reviewPageNumber) > 1 and float(starCount) >= 3:
                streetName = reviewCountSoup.find_all('span', class_='street-address')[0].get_text()
                localityName = reviewCountSoup.find_all('span', class_='locality')[0].get_text()
                localityName = localityName.split(',', 1)
                localityName = localityName[0]
                finalLocation = streetName + ', ' + localityName
                try:
                    finalLocation = re.escape(finalLocation)
                except TypeError:
                    finalLocation = finalLocation
                split_at = 'g187309'
                restName = restLink.split(split_at, 1)
                part_1 = restName[0] + 'g187309'
                split_at = '-Reviews-'
                restName = restName[1].split(split_at, 1)
                part_2 = restName[0] + '-Reviews-or'
                part_3 = '-' + restName[1]
                reviewLinkIndex = 0
                for y in range(2):
                    final_part = part_1 + part_2 + str(reviewLinkIndex) + part_3
                    ua3 = UserAgent()
                    randomHeader = {'User-Agent':str(ua3.random)}
                    finalReviewPage = requests.get(final_part, randomHeader)
                    finalReviewSoup = BeautifulSoup(finalReviewPage.content, 'html.parser')
                    partialEntryLen = len(finalReviewSoup.find_all('p', class_='partial_entry'))
                    for rp in range(partialEntryLen):
                        partialEntryReview = finalReviewSoup.find_all('p', class_='partial_entry')[rp].get_text()
                        try:
                            partialEntryReview = re.escape(partialEntryReview)
                        except TypeError:
                            partialEntryReview = partialEntryReview
                        print('Scraping data from: ', restNameList, finalLocation)
                        try:
                            cursor.execute("""INSERT INTO munich_list (link, address, name, stars, cuisine, review) VALUES ("%s", "%s", "%s", "%s", "%s", "%s")""" % (restLink, finalLocation, restNameList, starCount, cuisineType, partialEntryReview))
                        except:
                            db.rollback()
                    reviewLinkIndex += 10
            else:
                print('Skipping due to poor reviews or ratings!')
        except IndexError:
            print('Skipping due to poor reviews or ratings!')
    linkIndex += 30

end_time = time()
time_taken = end_time - start_time

print("Total time taken in seconds: ", time_taken)
