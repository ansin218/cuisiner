library(rvest)
library(stringr)

page0_url<-read_html ("https://www.tripadvisor.com/Restaurants-g297409-Lanzhou_Gansu.html")
npages<-page0_url%>% html_nodes(" .pageNum ") %>% html_attr(name="data-page-number") %>% tail(.,1) %>% as.numeric()

Restaurant_Name<-vector(mode="character", length=30*npages)
Restaurant_URL<-vector(mode="character", length=30*npages)

offset=0
idx_s=0

for (i in 1:npages)
{
  page_url<-paste("https://www.tripadvisor.com/Restaurants-g297409-oa",offset,"-Lanzhou_Gansu.html#EATERY_LIST_CONTENTS",sep="")
  link<-read_html(page_url)
  
  R_names<-link %>% html_nodes("a.property_title") %>% html_text() %>% gsub('[\r\n\t]', '', .)
  R_url<-link %>%  html_nodes("a.property_title") %>% html_attr(name="href")
  R_url<-paste("https://www.tripadvisor.com",R_url,sep="")
  R_count<-length( R_names)
  
  Restaurant_Name[(idx_s+1):(idx_s+R_count)]<-R_names
  Restaurant_URL[(idx_s+1):(idx_s+R_count)]<-R_url
  
  idx_s=idx_s+length(R_names)
  offset<-offset+30      
}

Restaurant_Name<-Restaurant_Name [Restaurant_Name!=""]
Restaurant_URL<-Restaurant_URL[Restaurant_URL!=""]
len=length(Restaurant_Name)
Restaurant_Cuisine<-vector(mode="character", length=len)

for(i in 1:len)
{
  rest_url<-Restaurant_URL[i]
  rest_cont<-read_html(rest_url)
  cuisine_nodes<-rest_cont %>% html_nodes("div.text") %>% html_text() %>% gsub('[\r\n\t]', '', .)
  cnl <- length(cuisine_nodes)
  if(cnl > 0) {
    if(startsWith(cuisine_nodes[cnl], ' ')) {
      Restaurant_Cuisine[i] <- str_trim(cuisine_nodes[cnl])
    } else {
      Restaurant_Cuisine[i]  <- 'Lookup'
    }
  } else {
    Restaurant_Cuisine[i]  <- 'Lookup'
  }
  print(i)
  print(rest_url)
  print(Restaurant_Cuisine[i])
}

#Restaurant_Cuisine<-Restaurant_Cuisine[Restaurant_Cuisine!=""]
ff<-data.frame(Restaurant_Name, Restaurant_Cuisine)
head(ff, 5)
write.table(ff,file="Lanzhou_Rest.csv", sep=",", row.names = F)