# -*- coding: utf-8 -*-
from nltk.tokenize import RegexpTokenizer
from stop_words import get_stop_words
from nltk.stem.porter import PorterStemmer
from gensim import corpora, models
import gensim
from time import time
import pandas as pd
import numpy as np

start_time = time()

df = pd.read_csv("vinpasa.csv")
#print(df["reviews"])

reviewList = list()
reviewList = df["reviews"].tolist()

tokenizer = RegexpTokenizer(r'\w+')
en_stop = get_stop_words('en')
p_stemmer = PorterStemmer()
doc_set = reviewList

customStopWordsList = ['food', 'restaurant', 'service', 'course', 'good', 'bad', 'even', 'tap']
en_stop = en_stop + customStopWordsList
#print(en_stop)

texts = []

for i in doc_set:
    raw = i.lower()
    tokens = tokenizer.tokenize(raw)
    stopped_tokens = [i for i in tokens if not i in en_stop]
    stemmed_tokens = [p_stemmer.stem(i) for i in stopped_tokens]
    texts.append(stemmed_tokens)

dictionary = corpora.Dictionary(texts)
corpus = [dictionary.doc2bow(text) for text in texts]
ldamodel = gensim.models.ldamodel.LdaModel(corpus, num_topics=1, id2word = dictionary, passes=20)

print(ldamodel.print_topics(20))

end_time = time()
time_taken = end_time - start_time

print("Total time taken in seconds: ", time_taken)
