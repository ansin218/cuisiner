import spacy
from time import time
import pandas as pd
import numpy as np
import en_core_web_sm
import json

nlp = en_core_web_sm.load()

start_time = time()

df = pd.read_csv("vinpasa.csv")
#print(df["reviews"])

reviewList = list()
reviewList = df["reviews"].tolist()

#nlp = spacy.load(“en”)

for i in range(len(reviewList)):
    doc = nlp(reviewList[i])
    print("Review ", i+1, ": ", reviewList[i])
    #for token in doc:
        #print("Word: ", token.orth_)
        #print("Upper Tag: ", token.pos_)
        #print("Lower Tag: ", token.tag_)
        #print(json.dumps({ "word":token.orth_, "upper_tag":token.pos_, "lower_tag":token.tag_ }))
    for ent in doc.ents:
        print(ent.text, ent.label_)
