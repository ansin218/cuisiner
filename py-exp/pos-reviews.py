from nltk import word_tokenize, pos_tag
from nltk.tokenize import RegexpTokenizer
from stop_words import get_stop_words
from nltk.stem.porter import PorterStemmer
from gensim import corpora, models
import gensim
from time import time
import pandas as pd
import numpy as np

start_time = time()

df = pd.read_csv("vinpasa.csv")
#print(df["reviews"])

reviewList = list()
reviewList = df["reviews"].tolist()

for i in range(len(reviewList)):
    nouns = [token for token, pos in pos_tag(word_tokenize(reviewList[i])) if pos.startswith('N')]
    print(nouns)

end_time = time()
time_taken = end_time - start_time

print("Total time taken in seconds: ", time_taken)
