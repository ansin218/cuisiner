#!/usr/bin/env python

import requests
from bs4 import BeautifulSoup
from time import time
import dryscrape
import pandas as pd

start_time = time()

linkIndex = 0

finalReviewList = list()
messageList = list()

for x in range(3):
    reviewLink = "https://www.tripadvisor.com/Restaurant_Review-g187309-d2182818-Reviews-or" + str(linkIndex) + "-Vinpasa-Munich_Upper_Bavaria_Bavaria.html"
    session = dryscrape.Session()
    session.visit(reviewLink)
    response = session.body()
    text_file = open("Output.txt", "w")
    text_file.write(response)
    text_file.close()
    print(reviewLink)
    try:
        #f = open('Output.txt','r')
        message = response
        #f.close()
        show_user_rev = '/ShowUserReviews'
        counter = 0
        if show_user_rev in message:
            message = message.split(show_user_rev, 1)
            message = message[1].split(".html#REVIEWS", 1)
            link = 'https://www.tripadvisor.com/ShowUserReviews' + message[0] + '.html#REVIEWS'
            messageList.append(link)
            for i in range(9):
                message = message[1]
                message = message.split(show_user_rev, 1)
                message = message[1].split(".html#REVIEWS", 1)
                innerLink = 'https://www.tripadvisor.com/ShowUserReviews' + message[0] + '.html#REVIEWS'
                messageList.append(innerLink)
    except IndexError:
        print("")
    linkIndex += 10

print("Length of reviews: ", len(messageList))

for i in range(20):
    reviewLink = messageList[i]
    session = dryscrape.Session()
    session.visit(reviewLink)
    response = session.body()
    #text_file = open("Output-Review.txt", "w")
    #text_file.write(response)
    #text_file.close()
    print(reviewLink)
    #f = open('Output-Review.txt','r')
    message = response
    #f.close()

    show_user_rev = '\"reviewBody\" : \"'

    if show_user_rev in message:
        message = message.split(show_user_rev, 1)
        message = message[1].split("\",\n\"reviewRating\"", 1)
        final_review = message[0]
        finalReviewList.append(final_review)

df = pd.DataFrame(finalReviewList, columns=["reviews"])
df.to_csv('vinpasa.csv', index=False)

print("CHECK vinpasa.csv NOW")
end_time = time()
time_taken = end_time - start_time

print("Total time taken in seconds: ", time_taken)
