from selenium import webdriver
#define driver- firefox, chrome  or phantomjs etc.
driver = webdriver.Firefox(executable_path='/usr/local/bin/geckodriver')
#Open the url
driver.get('https://www.google.com')
#see how javascript simple alert is being executed
driver.execute_script("alert('hello world');")
#close the driver  i.e. closing opened Firefox instance!
driver.close()
