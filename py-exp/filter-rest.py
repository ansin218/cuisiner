from time import time
import pandas as pd

start_time = time()

df = pd.read_csv('../csv-data/Munich_Rest.csv')

# Filtering begins here

df = df[df.Restaurant_Name != 'KFC']
df = df[df.Restaurant_Name != 'Burger King']
df = df[df.Restaurant_Name != 'McDonald\'s']
df = df[df.Restaurant_Name != 'Pizza Hut']
df = df[df.Restaurant_Name != 'Domino\'s Pizza']
df = df[df.Restaurant_Name != 'Subway']


df = df[df.Restaurant_Cuisine != 'Pub']

beforeDf = df[df.Restaurant_Cuisine == 'Lookup']
print("Before: ", len(beforeDf))

if df[df.Restaurant_Cuisine == 'Lookup']:
    if 'Pizz' in df[df.Restaurant_Name]:
        df[df.Cuisine] = 'Italian'

afterDf = df[df.Restaurant_Cuisine == 'Lookup']
print("After: ", len(afterDf))

end_time = time()
time_taken = end_time - start_time

print('Total time taken in seconds: ', time_taken)
