#!/usr/bin/env python

import requests
from bs4 import BeautifulSoup
from time import time
#import html5lib

start_time = time()

reviewList = list()

pageNumLink = "https://www.tripadvisor.com/Restaurant_Review-g187309-d2182818-Reviews-or10-Vinpasa-Munich_Upper_Bavaria_Bavaria.html"
page = requests.get(pageNumLink)
print(page.status_code)
soup = BeautifulSoup(page.content, 'html.parser')
pageLen = soup.find_all('a', class_='login-button')[6].get_text()
#print(soup.prettify())

#text_file = open("Output.txt", "w")
#text_file.write(soup.prettify())
#text_file.close()

end_time = time()
time_taken = end_time - start_time

print("Total time taken in seconds: ", time_taken)
